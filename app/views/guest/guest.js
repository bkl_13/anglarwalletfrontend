(function() {
    'use strict';
    var home = angular.module('anglar.guest', []);
    home.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $stateProvider.state('home', {
            url: '/home',
            abstract: true,
            data: {
                permissions: {
                    except: 'isUser',
                    redirectTo: 'dashboard.transaction'
                }
            },
            views: {
                'anglarWallet': {
                    templateUrl: 'app/views/guest/homePage.html',
                    controller: 'HomeController as home'
                }
            }
        }).state('home.login', {
            url: '/login',
            views: {
                'body': {
                    templateUrl: 'app/views/guest/loginPage.html',
                    controller: 'LoginController as login'
                }
            }
        })
        .state('home.register', {
            url: '/register',
            views: {
                'body': {
                    templateUrl: 'app/views/guest/registerPage.html',
                    controller: 'RegisterController as register'
                }
            }
        });
    }]);

    home.controller('HomeController', ['$auth', '$state', '$http', '$rootScope', function($auth, $state, $http, $rootScope) {
        var vm = this;
        vm.rootState = 'home.login';
        vm.currentState = '';
        vm.previousState = '';
        vm.showBackState = function() {
            return ((vm.getCurrentState() != vm.rootState) && vm.getPreviousState());
        }
        vm.getCurrentState = function() {
            return vm.currentState = $rootScope.currentState;
        };
        vm.getPreviousState = function() {
            return vm.previousState = $rootScope.previousState;
        }
    }]);

    home.controller('LoginController', ['$auth', '$state', '$http', '$rootScope', function($auth, $state, $http, $rootScope) {
        var vm = this;
        vm.hasError = false;
        vm.errorText;
        vm.loading = false;
        vm.submit = function() {
            if (vm.username && vm.password) {
                vm.loading = true;
                var credential = {
                    user_uname: vm.username,
                    user_pword: vm.password
                }
                $auth.login(credential).then(function() {
                    $http({
                        method: 'GET',
                        url: $rootScope.api + '/user/info'
                    }).then(vm.successUserInfo);
                }, vm.errorLogin);
            }
        }
        vm.successUserInfo = function(response) {
            var userObj = response.data.data.user;
            var user = JSON.stringify(userObj);
            localStorage.setItem('user', user);
            $rootScope.currentUser = JSON.parse(localStorage.getItem('user'));
            $state.go('dashboard.transaction');
            $rootScope.showSimpleToast('Xin chào, ' + userObj.user_firstname + "!");
        }
        vm.errorLogin = function(response) {
            vm.loading = false;
            vm.hasError = true;
            vm.errorText = response.data.error;
            console.log(response.data);
            $rootScope.showSimpleToast('Lỗi: ' + vm.errorText);
        }
    }]);

    home.controller('RegisterController', ['$auth', '$state', '$http', '$rootScope', function($auth, $state, $http, $rootScope) {
        var vm = this;
        vm.hasError = false;
        vm.errorText;
        vm.submit = function() {
            if (vm.username && vm.password && vm.firstname && vm.lastname && vm.email) {
                var userData = {
                    user_uname: vm.username,
                    user_pword: vm.password,
                    user_firstname: vm.firstname,
                    user_lastname: vm.lastname,
                    user_email: vm.email
                }

                $http({
                    method: 'POST',
                    url: $rootScope.api + "/user/signup",
                    data: userData
                }).then(vm.success, vm.error);
            }
        }
        vm.success = function(response) {
            $state.go('home.login');
            $rootScope.showSimpleToast('Đăng ký thành công');
        }
        vm.error = function(response) {
            $rootScope.showSimpleToast('Có lỗi trong quá trình đăng ký');
            vm.hasError = true;
            vm.errorText = response.data.message;

            var errors = response.data.errors;
            vm.usernameErrors = errors[0].user_uname;
            vm.passwordErrors = errors[0].user_pword;
            vm.lastnameErrors = errors[0].user_lastname;
            vm.firstnameErrors = errors[0].user_firstname;
            vm.emailErrors = errors[0].user_email;
        }
    }]);   
})();