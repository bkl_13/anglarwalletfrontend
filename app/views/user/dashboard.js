(function() {
    'use strict';
    var dashboard = angular.module('anglar.user', []);
    dashboard.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $stateProvider.state('dashboard', {
            url: '/dashboard',
            abstract: true,
            data: {
                permissions: {
                    only: 'isUser',
                    redirectTo: 'home.login'
                }
            },
            views: {
                'anglarWallet': {
                    templateUrl: 'app/views/user/dashboardPage.html',
                    controller: 'DashboardController as dashboard'
                }
            }
        }).state('dashboard.transaction', {
            url: '/transactions',
            views: {
                'main': {
                    templateUrl: 'app/views/user/transactionsPage.html',
                    controller: 'TransactionController as transaction'
                }
            }
        }).state('dashboard.debt', {
            url: '/debts',
            views: {
                'main': {
                    templateUrl: 'app/views/user/debtsPage.html',
                    controller: 'DebtController as debt'
                }
            }
        }).state('dashboard.wallet', {
            url: '/wallets',
            views: {
                'main': {
                    templateUrl: 'app/views/user/walletsPage.html',
                    controller: 'WalletController as wallet'
                }
            }
        }).state('dashboard.category', {
            url: '/categories',
            views: {
                'main': {
                    templateUrl: 'app/views/user/categoriesPage.html',
                    controller: 'CategoryController as cat'
                }
            }
        }).state('dashboard.account', {
            url: '/account',
            views: {
                'main': {
                    templateUrl: 'app/views/user/accountPage.html',
                    controller: 'AccountController as account'
                }
            }
        });
    }]);
    dashboard.controller('DashboardController', ['$http', '$auth', '$rootScope', '$state', '$q', function($http, $auth, $rootScope, $state, $q) {
        var vm = this;
    }]);

    dashboard.controller('TransactionController', ['$http', '$auth', '$rootScope', '$scope', '$state', '$q', '$filter', '$mdDialog', function($http, $auth, $rootScope, $scope, $state, $q, $filter, $mdDialog) {
        var vm = this;
        vm.loading = true;
        vm.error;
        $http.get($rootScope.api + '/transaction/index').success(function(trans) {
            vm.loading = false;
            vm.trans = trans.data[0].trans;
            console.log('Done for transactions');
        }).error(function(e) {
            vm.loading = false;
            vm.error = e.error;
            console.log(vm.error);
        });

        $scope.reload = function () {
            vm.loading = true;
            $http.get($rootScope.api + '/transaction/index').success(function(trans) {
                vm.loading = false;
                vm.trans = trans.data[0].trans;
                console.log('Done for transactions');
            }).error(function(e) {
                vm.loading = false;
                vm.error = e.error;
                console.log(vm.error);
            });
        }

        $scope.add = function(newTrans) {
            $http.post($rootScope.api + '/transaction/insert', {
                'cat_id': newTrans.cat_id,
                'wallet_id': newTrans.wallet_id,
                'trans_amount': newTrans.trans_amount,
                'trans_note': newTrans.trans_note,
                'trans_time': $filter('date')(newTrans.trans_time, 'yyyy-MM-dd HH:mm:ss')
            }).success(function() {
                $mdDialog.hide();
                $scope.reload();
            }).error(function(e) {
                vm.error = e.error;
                console.log(vm.error);
            });
        }
        vm.showAddDialog = function(ev) {
            vm.newTransaction = [];
            $mdDialog.show({
                templateUrl: 'app/views/user/addTransactionForm.html',
                parent: angular.element(transactionsPage),
                targetEvent: ev,
                clickOutsideToClose: true,
                controllerAs: 'transDlCtrl',
                controller: function TransactionDialogController($mdDialog) {
                    //Nen su dung gia tri scope cua controller cha thong qua vm de khong bi nham lan trong qua trinh lap trinh
                    var vm = this;
                    vm.xval = $scope;
                    vm.newTrans = [];
                    vm.cancel = function() {
                        $mdDialog.hide();
                    }
                    vm.add = function() {
                        vm.xval.add(vm.newTrans);
                    }
                    $http.get($rootScope.api + '/category/index').success(function(cats) {
                        vm.cats = cats.data[0].category;
                    }).error(function(e) {
                        vm.error = e.error;
                        console.log(vm.error);
                    });
                    $http.get($rootScope.api + '/wallet/index').success(function(wallets) {
                        vm.wallets = wallets.data[0].wallet;
                    }).error(function(e) {
                        vm.error = e.error;
                        console.log(vm.error);
                    });
                }
            });
        }
    }]);
    dashboard.controller('WalletController', ['$http', '$auth', '$rootScope', '$scope', '$state', '$q', '$mdDialog', function($http, $auth, $rootScope, $scope, $state, $q, $mdDialog) {
        var vm = this;
        vm.wallets = [];
        vm.loading = true;
        vm.error;
        $http.get($rootScope.api + '/wallet/index').success(function(wallets) {
            vm.loading = false;
            vm.wallets = wallets.data[0].wallet;
            console.log('Done for wallets');
        }).error(function(e) {
            vm.loading = false;
            vm.error = e.error;
            console.log(vm.error);
        });

        $scope.reload = function () {
            vm.loading = true;
            $http.get($rootScope.api + '/wallet/index').success(function(wallets) {
                vm.loading = false;
                vm.wallets = wallets.data[0].wallet;
                console.log('Done for wallets');
            }).error(function(e) {
                vm.loading = false;
                vm.error = e.error;
                console.log(vm.error);
            });
        }

        $scope.add = function(newWallet) {
            $http.post($rootScope.api + '/wallet/insert', {
                'wallet_name': newWallet.wallet_name,
                'wallet_description': newWallet.wallet_description,
                'start_point': newWallet.start_point,
            }).success(function() {
                $mdDialog.hide();
                $scope.reload();
            }).error(function(e) {
                vm.error = e.error;
                console.log(vm.error);
            });
        }
        vm.showAddDialog = function(ev) {
            vm.newWallet = [];
            $mdDialog.show({
                templateUrl: 'app/views/user/addWalletForm.html',
                parent: angular.element(walletsPage),
                targetEvent: ev,
                clickOutsideToClose: true,
                controllerAs: 'walletDlCtrl',
                controller: function WalletDialogController($mdDialog) {
                    var vm = this;
                    vm.xval = $scope;
                    vm.newWallet = [];
                    vm.cancel = function() {
                        $mdDialog.hide();
                    }
                    vm.add = function() {
                        vm.xval.add(vm.newWallet);
                    }
                }
            });
        }
    }]);
    dashboard.controller('CategoryController', ['$http', '$auth', '$rootScope', '$scope', '$state', '$q', '$mdDialog', function($http, $auth, $rootScope, $scope, $state, $q, $mdDialog) {
        var vm = this;
        vm.cats = [];
        vm.loading = true;
        vm.error;
        $http.get($rootScope.api + '/category/index').success(function(cats) {
            vm.loading = false;
            vm.cats = cats.data[0].category;
            console.log('Done for cats');
        }).error(function(e) {
            vm.loading = false;
            vm.error = e.error;
            console.log(vm.error);
        });

        $scope.reload = function () {
            vm.loading = true;
            $http.get($rootScope.api + '/category/index').success(function(cats) {
                vm.loading = false;
                vm.cats = cats.data[0].category;
                console.log('Done for cats');
            }).error(function(e) {
                vm.loading = false;
                vm.error = e.error;
                console.log(vm.error);
            });
        }

        $scope.add = function(newCat) {
            $http.post($rootScope.api + '/category/insert', {
                'cat_name': newCat.cat_name,
                'cat_type': newCat.cat_type,
            }).success(function() {
                $mdDialog.hide();
                $scope.reload();
            }).error(function(e) {
                vm.error = e.error;
                console.log(vm.error);
            });
        }

        vm.showAddDialog = function(ev) {
            vm.newWallet = [];
            $mdDialog.show({
                templateUrl: 'app/views/user/addCategoryForm.html',
                parent: angular.element(categoriesPage),
                targetEvent: ev,
                clickOutsideToClose: true,
                controllerAs: 'categoryDlCtrl',
                controller: function CategoryDialogController($mdDialog) {
                    var vm = this;
                    vm.xval = $scope;
                    vm.newCat = [];
                    vm.cancel = function() {
                        $mdDialog.hide();
                    }
                    vm.add = function() {
                        vm.xval.add(vm.newCat);
                    }
                }
            });
        }
    }]);
    dashboard.controller('AccountController', ['$http', '$auth', '$rootScope', '$state', '$q', function($http, $auth, $rootScope, $state, $q) {
        var vm = this;
        vm.loading = true;
        vm.editting = false;
        vm.error;
        $http.get($rootScope.api + '/user/info').success(function(user) {
            vm.loading = false;
            vm.user = user.data.user;
            console.log('Done for user');
        }).error(function(e) {
            vm.loading = false;
            vm.error = e.error;
            console.log(vm.error);
        });
    }]);
})();