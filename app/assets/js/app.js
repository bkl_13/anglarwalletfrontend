'use strict';
(function() {
    var anglar = angular.module('anglar', [
        'permission',
        'permission.ui',
        'ui.router',
        'anglar.guest',
        'anglar.user',
        'satellizer',
        'ngMaterial',
        'ngAnimate'
        ]);
    
    anglar.config(['$stateProvider', '$urlRouterProvider', '$authProvider', '$locationProvider', '$mdThemingProvider', function($stateProvider, $urlRouterProvider, $authProvider, $locationProvider, $mdThemingProvider) {
        // $locationProvider.html5Mode({
        //   enabled: true
        // });
        $mdThemingProvider.theme('blue')
          .primaryPalette('blue', {
            'default' : '500',
            'hue-1'   : '700',
            'hue-2'   : '800',
            'hue-3'   : 'A200'
        }).accentPalette('purple', {
              'default': '200' // use shade 200 for default, and keep all other shades the same
        });
        $mdThemingProvider.alwaysWatchTheme(true);
        $authProvider.loginUrl = 'http://wallet.io/public/api/v1/user/login';
        $urlRouterProvider.otherwise('/home/login');
    }]);

    anglar.run(function(PermissionStore, $rootScope, $state, $auth, $mdToast) {
        $rootScope.theme = 'blue';
        $rootScope.api = 'http://wallet.io/public/api/v1';
        $rootScope.currentUser = JSON.parse(localStorage.getItem('user'));

        PermissionStore.definePermission('isGuest', function() {
            return !$auth.isAuthenticated();
        });

        PermissionStore.definePermission('isUser', function() {
            return $auth.isAuthenticated();
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            $rootScope.currentState = toState.name;
            $rootScope.previousState = fromState.name;
        })

        $rootScope.logout = function() {
            $auth.logout().then(function() {
                localStorage.removeItem('user');
                $rootScope.currentUser = null;
                $state.go('home.login');
            })
        }
        $rootScope.showSimpleToast = function(message, position, delay) {
            position = typeof position !== 'undefined' ? position : 'bottom right';
            delay = typeof delay !== 'undefined' ? delay : 2000;
            var toast = $mdToast.simple().textContent(message).position(position).hideDelay(delay);
            $mdToast.show(toast);
        }
    });
})();